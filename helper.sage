def ext(Fl,Fs,Emb,mat):
    ncol = mat.ncols()
    nrow = mat.nrows()
    ext_deg = Emb.extension_degree()

    output = matrix(Fs,ext_deg*nrow,ncol)
    for jj in range(nrow):
        for ii in range(ncol):
            output[jj*ext_deg:(jj+1)*ext_deg,ii] = matrix(Fs,Emb.relative_field_representation(mat[jj,ii])).transpose()
    return output


def ext_inv(Fl,Fs,Emb,mat):

    ext_deg = Emb.extension_degree()
    ncol = mat.ncols()
    nrow = mat.nrows() / ext_deg

    output = matrix(Fl,nrow,ncol)

    if Fs.cardinality().is_prime():
        gamma = FE.absolute_field_basis()
        for jj in range(nrow):
            for ii in range(ncol):
                output[jj,ii] = sum([mat[jj*ext_deg+kk,ii]*a^kk for kk in range(ext_deg)])

    else:
        for jj in range(nrow):
            for ii in range(ncol):
                output[jj,ii] = Emb.absolute_field_representation(vector(Fq,mat[jj*ext_deg:(jj+1)*ext_deg,ii].list()))

    return output

def append_matrix_right(A,B):
    return matrix(list(A.transpose())+list(B.transpose())).transpose()

def my_rref(A):
    # Returns a matrix E and a matrix R s.t. R is the reduced ROW echelon form of A and E*A=R
    Fq = A[0,0].base_ring()
    A_tmp = append_matrix_right(A,matrix.identity(Fq,A.nrows()))
    R_tmp = A_tmp.rref()
    R = R_tmp[:,:A.ncols()]
    E = R_tmp[:,A.ncols():]
    return R, E
