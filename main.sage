from sage.coding.relative_finite_field_extension import *
load helper.sage

# Choose the desired parameters
q = 4
m = 16
n = 14
k = 7
d = (n-k+1)
t = floor( (d-1)/2 )
l = t

# Define fields
Fq = GF(q)
Fqm.<a> = GF(q^m)
FE = RelativeFiniteFieldExtension(Fqm, Fq)

# Random Gabidulin code
gs = matrix.random(Fqm,1,n)
while ext(Fqm,Fq,FE,gs).rank() < n:
        gs = matrix.random(Fqm,1,n)

G = matrix(Fqm, k, n, lambda i,j: gs[0,j]^(q^i))
H = G.right_kernel_matrix()

# Construction of the error E = A*B
B = matrix.random(Fq,t,n)
while B.rank()<t:
        B = matrix.random(Fq,t,n)

A = matrix.random(Fqm,l,t)
while A.rank()<t:
        A = matrix.random(Fqm,l,t)

E = ext_inv(Fqm,Fq,FE,ext(Fqm,Fq,FE,A)*B)

# Encode and Transmit over Channel
M = matrix.random(Fqm,l,k)
C = M*G
R = C + E

## Decoding with Algorithm 2
# 1. Compute Syndrome matrix
S = H*R.transpose()

# 2. Compute matrix P s.t. P*S == S.rref()
S_prime, P = my_rref(S)

# 3. Compute H_sub as the last n-k-t rows of P*H
H_sub = (P*H)[t:,:]

# 4. Determine B
B_hat = ext(Fqm,Fq,FE,H_sub).right_kernel_matrix()

# 5. Determine A
H_prime = ext_inv(Fqm,Fq,FE,ext(Fqm,Fq,FE,H)*B_hat.transpose())
A_hat = H_prime.solve_right(S).transpose()
E_hat = ext_inv(Fqm,Fq,FE, ext(Fqm,Fq,FE,A_hat) * B_hat)

# 6. Retrieve transmitted codeword
C_hat = R-E_hat

# Check if Algorithm
M_hat = G.solve_left(C_hat)

if not(B_hat.row_space()==B.row_space()):
        print "Estimation of the support of the error is wrong!"
else:
        print "Estimation of the support of the error is correct!"

if not(C_hat==C):
        print "Estimation of the codeword is wrong!"
else:
        print "Estimation of the codeword is correct!"
